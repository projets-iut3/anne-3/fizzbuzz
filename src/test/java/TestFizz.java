import org.junit.jupiter.api.Test;
import org.shiroling.FizzBuzzIsator;

import static org.junit.jupiter.api.Assertions.*;

public class TestFizz {
    @Test
    public void testCassic() {
        for (int i = 1; i < 100; i++) {
            if(i%3 != 0 && i%5 != 0) {
                assertEquals(String.valueOf(i), FizzBuzzIsator.giveAwnserForIndex(i));
            }
        }
    }

    @Test
    public void testFizz() {
        for (int i = 0; i < 100; i++) {
            if(i % 3 == 0 && i % 5 != 0)
                assertEquals("Fizz", FizzBuzzIsator.giveAwnserForIndex(i));
        }
    }
    @Test
    public void testBuzz() {
        for (int i = 0; i < 100; i++) {
            if (i % 5 == 0 && i % 3 != 0)
                assertEquals("Buzz", FizzBuzzIsator.giveAwnserForIndex(i));
        }
    }
    @Test
    public void testFizzBuzz() {
        for (int i = 0; i < 100; i++) {
            if(i%3 == 0 && i%5 == 0)
                assertEquals("FizzBuzz", FizzBuzzIsator.giveAwnserForIndex(i));
        }
    }
}
